In general, computer systems support single (and mostly double) precision for calculations. Overflows are common when results exceed the precision sizes. Arbitrary-precision or multi-precision arithmetic partition the arbitrary numbers into digit arrays and perform the arithmetic calculations on the digit arrays.

- Create a bigPosInt class using dynamic arrays.
- overload +, - and * operators to add, subtract and multiply bigPosInt numbers.
- overload >> and << for easy io.
- implement appropriate constructors.

main.cpp shall support the following statements:

bigPosInt num1(“1234567890123456789012345”); 
bigPosInt num2(“11223344556677889900112233"); ; 
cin >> num1;

cout << “num1: ” << num1 << endl;
cout << “num2: ” << num2 << endl;

numTest = num1 + num2;
cout << “numTest: ” << numTest << endl;

numTest = num1 * num2;
cout << “numTest: ” << numTest << endl;

numTest = num1 - num2;
cout << “numTest: ” << numTest << endl;